#!/bin/sh -e

VERSION=$2
TAR=../jenkins-htmlunit-core-js_$VERSION.orig.tar.gz
DIR=jenkins-htmlunit-core-js-$VERSION
mkdir -p $DIR
# Expand the upstream tarball
tar -xzf $TAR -C $DIR --strip-components=1
# Repack excluding stuff we don't need
# and ensure checksums match when run 
# in different places
# Fix date for file creation - allows consistent
# generating of tarballs from github.com tags.
DATE="Fri Oct 21 12:29:52 BST 2011"
tar -c --exclude '*.jar' --exclude '*.class' \
  --exclude 'CVS' --exclude '.svn' --exclude '.git' \
  --mtime="$DATE" $DIR | gzip -9fn -c - > $TAR
                                                                               
rm -rf $DIR

